from __future__ import annotations
import logging

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional

if TYPE_CHECKING:
    pass

def hello_world(name="world"):
    return {"hello": name}
