# Install

from pip:

*TBD*

from source:

* clone this repo

```bash
pip install -e .
```

# Usage

run on your connector

```bash
cd path/to/connector
python -m mium_sdk.server
```

## CLI Arguments

* `-p` port to run the server on (default: 5000)

## Client

```bash
python -m mium_sdk.client
```

### CLI Arguments

* `-H` host to connect to (default: localhost)
* `-p` port to connect to (default: 5000)
* `-f` function name to call (default: hello.hello_world), e.g. `hello.hello_world` for a function called `hello_world` in a file called `hello.py`
* `-a` arguments to pass to the function, e.g. `-a name=world` for a function that takes a `name` argument
