from __future__ import annotations

import argparse
import logging

from mium_sdk.server import RPCServer

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from icecream import ic

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional

if TYPE_CHECKING:
    pass

import sys
import logging
logger = logging.getLogger()
sh = logging.StreamHandler(sys.stdout)
sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(name)-12s %(message)s", datefmt='%Y-%m-%d %H:%M:%S'))
logger.addHandler(sh)
logger.setLevel(logging.DEBUG)

parser = argparse.ArgumentParser(description='')
parser.add_argument('-H', dest='host', type=str, default='localhost', help='host')
parser.add_argument('-p', dest='port', type=int, default=5000, help='port')
parser.add_argument('-f', dest='function_name', type=str, default='hello.hello_world', help='function_name')
parser.add_argument('-a', dest='args', type=str, default=None, help='args', nargs='*', action='append')

args = parser.parse_args()

def main():
    from mium_sdk.client import RPCClient
    client = RPCClient(host=args.host, port=args.port)
    client.connect()

    # args might be sth. like: [['foo=bar,a=7'], ['b=3']]
    # convert args to dict
    args_dict = {}
    if args.args is not None:
        for arg in args.args:
            for arg_pair in arg:
                for arg_element in arg_pair.split(","):
                    key, value = arg_element.split("=")
                    args_dict[key] = value
    # args_dict is now {'foo': 'bar', 'a': '7', 'b': '3'}

    result = client.call(function_name=args.function_name, args=args_dict)
    ic(result)

main()