from __future__ import annotations

import logging
import socket

import msgpack

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from icecream import ic

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional, Any

if TYPE_CHECKING:
    pass

class RPCClient:
    def __init__(self, host: str, port: int = 5000):
        self.host = host
        self.port = port

        self.s: socket.socket = None

    def connect(self):
        # create a socket object
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # connection to hostname on the port.
        self.s.connect((self.host, self.port))

    def send_payload(self, data: Dict) -> Dict:
        payload_bytes = msgpack.packb(data)
        self.s.sendall(payload_bytes)

        # Receive no more than 1024 bytes
        result = self.s.recv(1024)
        return msgpack.unpackb(result, use_list=False)

    def close(self):
        self.s.close()

    def call(self, function_name: str, args: Dict[str, Any] = None):
        data = {
            "function_name": function_name,
        }
        if args is not None:
            data["args"] = args

        result = self.send_payload(data)
        return result