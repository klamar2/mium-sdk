from __future__ import annotations

import argparse
import logging

from mium_sdk.server import RPCServer

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from icecream import ic

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional

if TYPE_CHECKING:
    pass

import sys
import logging
logger = logging.getLogger()
sh = logging.StreamHandler(sys.stdout)
sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(name)-12s %(message)s", datefmt='%Y-%m-%d %H:%M:%S'))
logger.addHandler(sh)
logger.setLevel(logging.DEBUG)

parser = argparse.ArgumentParser(description='')
parser.add_argument('-H', dest='host', type=str, default='localhost', help='host')
parser.add_argument('-p', dest='port', type=int, default=5000, help='port')
parser.add_argument('-t', dest='run_time', type=int, default=5, help='run_time')
parser.add_argument('-f', dest='function_name', type=str, default='hello.hello_world', help='function_name')

args = parser.parse_args()

import time

def main():
    from mium_sdk.client import RPCClient
    client = RPCClient(host=args.host, port=args.port)
    client.connect()

    logger.info("checking if everything works")
    result = client.call(function_name=args.function_name)
    if result["success"] is not True:
        ic(result)
        logger.error(f"Error: {result}")
        exit(1)

    logger.info(f"everything works, starting benchmark, run_time: {args.run_time} seconds")

    start_time = time.time()
    end_time = start_time + args.run_time
    call_count = 0

    while time.time() < end_time:
        result = client.call(function_name=args.function_name)
        if result["success"] is not True:
            ic(result)
            logger.error(f"Error: {result}")
            exit(1)
        call_count += 1

    elapsed_time = time.time() - start_time
    calls_per_second = call_count / elapsed_time

    logger.info(f"benchmark finished, elapsed_time: {elapsed_time:.2f}, call_count: {call_count}, calls_per_second: {calls_per_second:.2f}")

main()