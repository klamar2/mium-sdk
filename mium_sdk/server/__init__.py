from __future__ import annotations

import json
import logging
import os
import socket
import traceback
from importlib import import_module

import msgpack
from msgpack import UnpackValueError

from mium_sdk.helper import Capturing

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from icecream import ic

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional, Any

if TYPE_CHECKING:
    pass

BUFSIZE = 65536
PROTOCOL_VERSION = 1

class RPCServer:
    # maps function strings to their function objects
    function_cache = {}

    def __init__(self, port: int):
        self.port = port

    def handle_payload(self, payload: bytes) -> bytes:
        try:
            # use_list returns tuples instead of list objects -> more performance
            data = msgpack.unpackb(payload, use_list=False)
        except UnpackValueError as e:
            if len(payload) < 1024:
                import base64
                data_repr = base64.b64encode(payload).decode()
            else:
                data_repr = f"[to-large-to-print]"

            logger.warning(f"Received extra data: {data_repr}")
            return msgpack.packb({
                "success": False,
                "result": None,
                "output": f"Internal Error: unable to decode msgpack: (base64) {data_repr}",
            })

        result = self.handle_message(data)
        return msgpack.packb(result)

    def listen(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # get local machine name
        # host = socket.gethostname()
        host = "0.0.0.0"

        # bind the socket to a public host, and a port
        logger.info(f"Binding to {host}:{self.port} protocol version {PROTOCOL_VERSION}")
        self.socket.bind((host, self.port))

        # become a server socket
        self.socket.listen(1)

        try:
            while True:
                # establish a connection
                client_socket, addr = self.socket.accept()
                logger.info(f"Got a connection from {addr}")

                with client_socket:
                    while True:
                        # Initialize an empty bytes object to hold the full response
                        request_payload = b''

                        # receive data from the client
                        header = client_socket.recv(5)

                        if header == b'':
                            client_socket.close()
                            break

                        # In Python, when you index a bytes object, you get an integer.
                        # This is because bytes are sequences of integers in the range of 0-255
                        # As this notation already uses big ending byte order and is unsigned we do not have to
                        # convert the bytes to integer by ourselves.
                        version: int = header[0]
                        if version != 1:
                            try:
                                client_socket.close()
                            except Exception as e:
                                pass
                            raise ValueError(f"Unsupported protocol version {version}")

                        # header bytes 2-5 (4 bytes) contain the payload length
                        payload_length_bytes = header[1:]
                        payload_length = int.from_bytes(payload_length_bytes, byteorder='big', signed=False)

                        # Loop until the full response has been received
                        while len(request_payload) < payload_length:
                            # Receive up to 64 kilobytes of data
                            chunk = client_socket.recv(65536)

                            # If no more data is received, break the loop
                            if not chunk:
                                break

                            # Append the received data to the full response
                            request_payload += chunk

                        result_payload = self.handle_payload(request_payload)

                        payload_length = len(result_payload)
                        payload_length_bytes = payload_length.to_bytes(4, byteorder='big', signed=False)

                        # prepend the version byte to the payload length bytes
                        version = PROTOCOL_VERSION
                        version_byte = version.to_bytes(1, byteorder='big', signed=False)

                        # 5 bytes header: 1 byte version + 4 bytes payload length
                        header = version_byte + payload_length_bytes

                        client_socket.send(header)
                        client_socket.send(result_payload)
        finally:
            self.socket.close()

    def handle_message(self, data: Dict[str, Any]) -> Dict[str, Any]:
        """
        :param data: {'args': {'x': 'Hello World'}, 'function_name': 'return_x'}
        :return:
        """
        logger.debug(f"handle_message(): {json.dumps(data)}")
        # get the function name and arguments from the data
        function_string = data["function_name"]

        try:
            function = self.function_cache[function_string]
        except KeyError:
            module_name, function_name = function_string.rsplit('.', 1)
            try:
                module = import_module(module_name)
            except ModuleNotFoundError as e:
                return {
                    "success": False,
                    "result": None,
                    "output": f"Module {module_name!r} not found",
                }
            # import the function from a module
            try:
                function = getattr(module, function_name)
            except AttributeError:
                return {
                    "success": False,
                    "result": None,
                    "output": f"Function {function_name!r} not found",
                }
            self.function_cache[function_string] = function

        args = data.get("args", {})

        if "env" in data:
            os.environ.update(data["env"])

        # execute the function with the arguments
        try:
            with Capturing() as output:
                result = function(**args)

            return {"result": result, "output": "\n".join(output), "success": True}

        except Exception as e:
            # log traceback
            tb = traceback.format_exc()
            logger.error(f"Error in function {function_string}. {e.__class__.__name__}: {e}."
                         f" Traceback:\n{tb}")

            return {"result": None, "output": str(e), "success": False}
