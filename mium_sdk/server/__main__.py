from __future__ import annotations

import argparse
import logging
import os
from os.path import isdir

from mium_sdk.server import RPCServer

logger = logging.getLogger(__name__)

# noinspection PyUnresolvedReferences
from icecream import ic

# noinspection PyUnresolvedReferences
from typing import TYPE_CHECKING, Union, Dict, List, Optional

if TYPE_CHECKING:
    pass

import sys
import logging
logger = logging.getLogger()
sh = logging.StreamHandler(sys.stdout)
sh.setFormatter(logging.Formatter("%(asctime)s %(levelname)-8s %(name)-12s %(message)s", datefmt='%Y-%m-%d %H:%M:%S'))
logger.addHandler(sh)
logger.setLevel(logging.DEBUG)

parser = argparse.ArgumentParser(description='')
parser.add_argument('-p', dest='port', type=int, default=5000, help='port')

args = parser.parse_args()

ENVIRONMENT_VARS_WHITELIST = {
    "HOME",
    "LANG",
    "PATH",
    "PWD",
    "TERM",
    "_",
}

def clean_environment():
    # remove all environment variables except the whitelisted ones
    for var in list(os.environ.keys()):  # use list to avoid RuntimeError: dictionary changed size during iteration
        if var not in ENVIRONMENT_VARS_WHITELIST:
            del os.environ[var]

    os.environ.update({
        "HOST": "mium",
    })

def main():
    clean_environment()

    if isdir("src"):
        sys.path.insert(0, "src")

    server = RPCServer(port=args.port)
    server.listen()

main()